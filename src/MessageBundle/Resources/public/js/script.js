// take data from form and send to server
$('#message_send').click(function (e) {
    e.preventDefault();
    data = $('form').serializeArray();
    $.ajax({
        method: 'POST',
        url: '/add-message',
        data: {text: data[0]['value']},
    }).done(function (result) {
        resultData = result;
        var html;
        $('.table tbody').html('');
        for (var i = 0; i < resultData.length; i++) {
            html += "<tr><td>" + resultData[i].created_at + "</td><td>" + resultData[i].text + "</td></tr>"
        }
        $('.table tbody').html(html);
    });

});