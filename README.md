test-zen
========

A Symfony project created on August 30, 2016, 09:53 am.

To install project go through these steps in command line:

$ bin/composer.phar install

$ bin/console doctrine:database:create

$ bin/console doctrine:schema:create

$ bin/console server:start

Go to http://127.0.0.1:8000 in your browser







