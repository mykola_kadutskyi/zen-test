<?php

namespace MessageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Message
 *
 * @ORM\Table(name="message")
 * @ORM\Entity(repositoryClass="MessageBundle\Repository\MessageRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Message {
	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="textMessage", type="text")
	 *
	 */
	private $textMessage;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="createdAt", type="datetime", nullable=true)
	 */
	private $createdAt;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="updatedAt", type="datetime", nullable=true)
	 */
	private $updatedAt;


	/**
	 * Get id
	 *
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set textMessage
	 *
	 * @param string $textMessage
	 *
	 * @return Message
	 */
	public function setTextMessage( $textMessage ) {
		$this->textMessage = $textMessage;

		return $this;
	}

	/**
	 * Get textMessage
	 *
	 * @return string
	 */
	public function getTextMessage() {
		return $this->textMessage;
	}

	/**
	 * Set createdAt
	 *
	 * @param \DateTime $createdAt
	 *
	 * @return Message
	 */
	public function setCreatedAt( $createdAt ) {
		$this->createdAt = $createdAt;

		return $this;
	}

	/**
	 * Get createdAt
	 *
	 * @return \DateTime
	 */
	public function getCreatedAt() {
		return $this->createdAt;
	}

	/**
	 * Set updatedAt
	 *
	 * @param \DateTime $updatedAt
	 *
	 * @return Message
	 */
	public function setUpdatedAt( $updatedAt ) {
		$this->updatedAt = $updatedAt;

		return $this;
	}

	/**
	 * Get updatedAt
	 *
	 * @return \DateTime
	 */
	public function getUpdatedAt() {
		return $this->updatedAt;
	}

	/**
	 * @ORM\PrePersist
	 */
	public function setCreatedAtValue() {
		$this->createdAt = new \DateTime();
	}

	/**
	 * @ORM\PreUpdate
	 */
	public function setUpdatedAtValue() {
		$this->updatedAt = new \DateTime( "now" );
	}
}


