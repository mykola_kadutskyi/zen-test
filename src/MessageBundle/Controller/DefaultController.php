<?php

namespace MessageBundle\Controller;

use MessageBundle\Entity\Message;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use MessageBundle\Form\MessageType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller {


	/**
	 * Renders front page with messages
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function indexAction() {

		$em       = $this->getDoctrine()->getManager();
		$messages = $em->getRepository( 'MessageBundle:Message' )->findAll();


		$message = new Message();
		$form    = $this->createForm( MessageType::class, $message );


		return $this->render( 'MessageBundle:Default:index.html.twig', [
			'messages' => array_reverse( $messages ),
			'form'     => $form->createView(),
		] );
	}

	public function addMessageAction( Request $request ) {

		$m_params    = $this->container->getParameter( 'stored_messages' );

		$isAjax = $request->isXmlHttpRequest();
		if ( $isAjax ) {
			$message = $request->get( 'text' );

			$em          = $this->getDoctrine()->getManager();
			$messages    = $em->getRepository( 'MessageBundle:Message' )->findAll();
			$new_message = new Message();
			$new_message->setTextMessage( $message );

			if ( count( $messages ) == $m_params['quantity'] ) {
				$em->remove( $messages[0] );
			}

			$em->persist( $new_message );
			$em->flush();
			$messages = $em->getRepository( 'MessageBundle:Message' )->findAll();
			foreach ( $messages as $item ) {
				$prep_m[] = array(
					'created_at' => $item->getCreatedAt()->format( 'Y-m-d G:i:s' ),
					'text'       => $item->getTextMessage()
				);
			}
			$response = new Response( json_encode( array_reverse( $prep_m ) ) );

			return $response;
		}
	}

}
